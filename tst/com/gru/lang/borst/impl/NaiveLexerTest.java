package com.gru.lang.borst.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.gru.lang.borst.Lexer;
import com.gru.lang.borst.Token;
import com.gru.lang.borst.TokenType;

public class NaiveLexerTest {
	
	Lexer naiveLexer = null;
	
	public NaiveLexerTest() {
		naiveLexer = new NaiveLexer();
	}
	
	/**
	 * 
	 */
	@Test
	public void testNumberRecognition() {
		Lexer lexer = new NaiveLexer();
		List<Token> tokens = lexer.lex("123");
		assertEquals(TokenType.number, tokens.get(0).type());
		tokens = lexer.lex(" 898  123");
		assertEquals(TokenType.number, tokens.get(0).type());
		tokens = lexer.lex("     34   234  999");
		assertEquals(TokenType.number, tokens.get(0).type());
		assertEquals(TokenType.number, tokens.get(1).type());
		assertEquals(Integer.valueOf(234), Integer.valueOf((int)tokens.get(1).value()));
		assertEquals(Integer.valueOf(999), Integer.valueOf((int)tokens.get(2).value()));
		
		tokens = lexer.lex("foobar 324");
		assertEquals(Integer.valueOf(324), Integer.valueOf((int)tokens.get(1).value()));
		
	}
	
	@Test
	public void testIds() {
		var tokens = naiveLexer.lex("foobar 324");
		assertEquals(TokenType.id, tokens.get(0).type());
		assertEquals("foobar", String.valueOf(tokens.get(0).value()));
		assertEquals(324, (int)tokens.get(1).value());
		
	}
	
	@Test
	public void testSingleChars() {
		var tokens = naiveLexer.lex("& | =   \n");
		assertEquals(TokenType.ampersand, tokens.get(0).type());
		assertEquals("&", tokens.get(0).value());
		assertEquals(TokenType.pipe, tokens.get(1).type());
		assertEquals("|", tokens.get(1).value());
		assertEquals(TokenType.equals, tokens.get(2).type());
		assertEquals("=", tokens.get(2).value());
		assertEquals(TokenType.lineFeed, tokens.get(3).type());
		
		
	}
}
