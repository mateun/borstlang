package com.gru.lang.borst.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.gru.lang.borst.BorstAst;
import com.gru.lang.borst.BorstEnvironment;
import com.gru.lang.borst.Lexer;
import com.gru.lang.borst.Parser;
import com.gru.lang.borst.Token;
import com.gru.lang.borst.TokenType;
import com.gru.lang.borst.ast.TokenBuilder;

public class NaiveParserTest {
	
	Parser naiveParser = null;
	Lexer lexer = null;
	BorstEnvironment env = null;
	
	public NaiveParserTest() {
		naiveParser = new NaiveParser();
		lexer = new NaiveLexer();
		env = new BorstEnvironment() {
			
			Map<String, Object> vals = new HashMap<>();
			
			@Override
			public void storeValue(String name, Object value) {
				vals.put(name, value);
				
			}
			
			@Override
			public Object getValue(String name) {
				return vals.get(name);
			}
		};
	}
	
	@Test
	public void testMulPlus() {
		var tokens = Arrays.asList(new Token(TokenType.number, 5), 
				new Token(TokenType.plus, "+"), 
				new Token(TokenType.number, 3), 
				new Token(TokenType.mul, null), 
				new Token(TokenType.number, 5),
				new Token(TokenType.mul, null),
				new Token(TokenType.number, 2),
				new Token(TokenType.plus, null),
				new Token(TokenType.number, 3));
		BorstAst ast = naiveParser.parse(tokens);
		assertNotNull(ast);
		Integer result = (Integer)ast.evaluate(env);
		assertNotNull(result);
		assertEquals(38, (int)result);
		
		
	}
	
	@Test
	public void testMinus() {
		var tokens = Arrays.asList(new Token(TokenType.number, 5), 
				new Token(TokenType.minus, null),
				new Token(TokenType.number, 3));
		
		var ast = naiveParser.parse(tokens);
		assertNotNull(ast);
		var result = (Integer)ast.evaluate(env);
		assertEquals(2, (int)result);
		
	}
	
	
	@Test
	public void testMulMinus() {
		var tokens = new TokenBuilder().lparen().num(5).minus(3).mul(2).rparen().plus(10).build();
		var ast = naiveParser.parse(tokens);
		assertNotNull(ast);
		var result = (Integer)ast.evaluate(env);
		assertEquals(9, (int)result);
	}
	
	@Test
	public void testMulMinus2() {
		var tokens = new TokenBuilder().num(5).mul().lparen().num(2).plus(10).rparen().build();
		var ast = naiveParser.parse(tokens);
		assertNotNull(ast);
		var result = (Integer)ast.evaluate(env);
		assertEquals(60, (int)result);
	}

}
