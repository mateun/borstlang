package com.gru.lang.borst.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.gru.lang.borst.BorstAst;
import com.gru.lang.borst.Parser;
import com.gru.lang.borst.Token;
import com.gru.lang.borst.TokenType;
import com.gru.lang.borst.ast.AstNode;
import com.gru.lang.borst.ast.TermNode;

public class NaiveParser implements Parser {
	
	List<Token> tokens = null;
	Token current = null;
	int currentIndex = 0;

	@Override
	public BorstAst parse(List<Token> tokens) {
		this.tokens = tokens;
		return program();
	}
	
	// This is where everything starts.
	// Every borst unit must be a program.
	// A program is currently a single expression.
	// Next step may be to recognize a list of expressions, statement(s) etc.
	private ProgramNode program() {
		AstNode expr = expression();
		return new ProgramNode(expr);
		
	}
	
	private AstNode expression() {
		AstNode lhs = null;
		if (matchType(lookAhead(0), TokenType.number)) {
			 lhs = term();
		}
		
		boolean done = false;
		while (!done) {
			
			if (matchType(lookAhead(0), TokenType.lparen)) {
				getCurrent();
				lhs = expression();
			}
			
			if (matchType(lookAhead(0), TokenType.rparen)) {
				getCurrent();
				done = true;
				break;
			}

			
			
			if (matchType(lookAhead(0), TokenType.lineFeed)) {
				done = true;
				break;
			}
			else if (matchType(lookAhead(0), TokenType.plus)) {
				currentIndex+=1;
				return new AdditionNode(lhs, expression());
			}
			else if (matchType(lookAhead(0), TokenType.minus)) {
				currentIndex+=1;
				return new SubtractNode(lhs, expression());
			} else {
				done = true;
			}
		}
		
		return lhs;
	}		
	
	
	AstNode subExpr() {
		
		
		
		for(;;) {
			
			return expression();
		}
	}

	
	
	
	Optional<Token> lookAhead(int offset) {
		if (tokens.size()>((currentIndex+offset))) {
			return Optional.of(tokens.get(currentIndex + offset));
		} else {
			return Optional.empty();
		}
	}
	
	boolean matchType(Optional<Token> test, TokenType type) {
		return test.filter(t -> t.type().equals(type)).isPresent();
	}
	
	boolean matchAnyType(Optional<Token> test, List<TokenType> types) {
		return test.filter(t -> types.contains(t.type())).isPresent();
	}
	
	// Returns and eats the current token.
	Token getCurrent() {
		return tokens.get(currentIndex++);
	}
	
	private AstNode term() {
		Integer lhsVal = null;
		if (matchType(lookAhead(0), TokenType.number)) {
			lhsVal = resolveNumber(getCurrent());
		}
		
		if (matchType(lookAhead(0), TokenType.lparen)) {
			getCurrent();
			return expression();
		}
		
		if (matchType(lookAhead(0), TokenType.rparen)) {
			if (lhsVal != null) {
				return new NumNode(lhsVal);
			} else {
				getCurrent();
			}
		}

		
		if (matchType(lookAhead(0), TokenType.mul)) {
			getCurrent();
			return new MulNode(new NumNode(lhsVal), term());	
		} else  {
			return new NumNode(lhsVal);
		}
	}

	boolean opAnyOf(List<TokenType> tokenTypes, Token test) {
		return (tokenTypes.contains(test.type()));
	}
	

	private Integer resolveNumber(Token curr) {
		if (!curr.type().equals(TokenType.number)) {
			throw new RuntimeException("parse error: expected number but got: " + curr.type());
		}
		
		return Integer.valueOf((int)curr.value());
	}

}
