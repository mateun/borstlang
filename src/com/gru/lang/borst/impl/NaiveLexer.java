package com.gru.lang.borst.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.gru.lang.borst.Lexer;
import com.gru.lang.borst.Token;
import com.gru.lang.borst.TokenType;

public class NaiveLexer implements Lexer {
	
	public static record LexerError(String failingWord) {}
	public static class LexerException extends RuntimeException  {
		
		private final LexerError err;

		public LexerError getErr() {
			return err;
		}

		LexerException(LexerError err) {
			this.err = err;
			
		}
	}
	

	@Override
	public List<Token> lex(String sourceCode) {
		var tokens = new ArrayList<Token>();
		
		List<String> words = Arrays.asList(sourceCode.split(" ")).stream().filter(w->!w.isBlank() || "\n".equals(w)).collect(Collectors.toList());
		
		// We now have a list of words. 
		// For every word we find out what kind of token it is. 
		// If we can not find a valid token type, 
		// we throw an error. 
		// Otherwise we create a token, assign it the 
		// correct token type, remember the actual value 
		// and add the token to the token list.
		for (var w : words) {
			
			// Do we start with a number?
			// If yes, we will try to resolve the whole word 
			// as a number.
			// We only know integers currently.
			if (Character.isDigit(w.charAt(0))) {
				try {

					Integer intVal = Integer.valueOf(w);
					tokens.add(new Token(TokenType.number, intVal));
				} catch (NumberFormatException nfex) {
					throw new LexerException(new LexerError(w));
				}
			}
			
			// Identify single character tokens
			else if (w.length() == 1) {
				resolveSingleChar(w).ifPresent(sct -> tokens.add(sct));
			}
		
			
			// Double character tokens (e.g. &&)
			
			// If we start with a letter we assume this is either a keyword
			// or an identifier.
			else if (Character.isLetter(w.charAt(0))) {
				resolveKeyword(w).ifPresent(kwt -> tokens.add(kwt));
				resolveId(w).ifPresent(idt -> tokens.add(idt));
			}
			
			
			
		}
		
		return tokens;
	}


	private Optional<Token> resolveSingleChar(String w) {
		switch(w) {
		case "&": return Optional.of(new Token(TokenType.ampersand, w));
		case "|": return Optional.of(new Token(TokenType.pipe, w));
		case "=": return Optional.of(new Token(TokenType.equals, w));
		case "\n" : return Optional.of(new Token(TokenType.lineFeed, null));
		}
		
		return Optional.empty();
	}


	private Optional<Token> resolveId(String w) {
		// TODO check for any illegal characters
		return Optional.of(new Token(TokenType.id, w));
	}


	private Optional<Token> resolveKeyword(String w) {
		
		switch (w) {
		case "if":  return Optional.of(new Token(TokenType.if_keyword, null));
		case "class": return Optional.of(new Token(TokenType.class_keyword, null));
		}
		return Optional.empty();
	}
	
	

}
