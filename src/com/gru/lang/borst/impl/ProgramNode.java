package com.gru.lang.borst.impl;

import com.gru.lang.borst.BorstEnvironment;
import com.gru.lang.borst.ast.AstNode;

public class ProgramNode implements AstNode {

	private AstNode root;

	public ProgramNode(AstNode expr) {
		this.root = expr;
	}

	@Override
	public Object evaluate(BorstEnvironment env) {
		return root.evaluate(env);
	}

}
