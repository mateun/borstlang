package com.gru.lang.borst.impl;

import com.gru.lang.borst.BorstEnvironment;
import com.gru.lang.borst.ast.AstNode;

public class NumNode implements AstNode {
	
	private int val;

	public NumNode(int val) {
		this.val = val;
		
	}

	@Override
	public Object evaluate(BorstEnvironment env) {
		return val;
	}

}
