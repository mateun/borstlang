package com.gru.lang.borst.impl;

import com.gru.lang.borst.BorstEnvironment;
import com.gru.lang.borst.ast.AstNode;

public class SubExpression implements AstNode {
	
	private AstNode subExpr;

	public SubExpression(AstNode subExpr) {
		this.subExpr = subExpr;
	}

	@Override
	public Object evaluate(BorstEnvironment env) {
		return subExpr.evaluate(env);
	}

}
