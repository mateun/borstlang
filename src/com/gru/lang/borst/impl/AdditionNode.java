package com.gru.lang.borst.impl;

import com.gru.lang.borst.BorstEnvironment;
import com.gru.lang.borst.ast.AstNode;

public class AdditionNode extends ExprNode {

	private AstNode lhs;
	private AstNode rhs;

	public AdditionNode(AstNode lhs, AstNode rhs) {
		this.lhs = lhs;
		this.rhs = rhs;
		}
	
	@Override
	public Object evaluate(BorstEnvironment env) {
		return (int)lhs.evaluate(env) + (int)rhs.evaluate(env);
	}

}
