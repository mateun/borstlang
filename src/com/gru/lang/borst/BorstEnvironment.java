package com.gru.lang.borst;

public interface BorstEnvironment {
	
	
	void storeValue(String name, Object value);
	Object getValue(String name);

}
