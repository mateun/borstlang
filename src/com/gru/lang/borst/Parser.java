package com.gru.lang.borst;

import java.util.List;

public interface Parser {
	
	BorstAst parse(List<Token> tokens);

}
