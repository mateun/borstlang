package com.gru.lang.borst;

import java.util.List;



public interface Lexer {
	
	List<Token> lex(String sourceCode); 

}
