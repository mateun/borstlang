package com.gru.lang.borst;

public enum TokenType {
	
	plus,
	minus,
	mul,
	div,
	percent,
	hash,
	equals, 
	gt,
	lt,
	le,
	ge,
	
	lineFeed,
	
	ampersand,
	pipe,
	
	number,
	id, 
	
	lparen,
	rparen,
	
	if_keyword,
	while_keyword,
	class_keyword,
	do_keyword,
	

}
