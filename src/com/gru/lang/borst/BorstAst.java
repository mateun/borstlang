package com.gru.lang.borst;

public interface BorstAst {
	
	Object evaluate(BorstEnvironment env);

}
