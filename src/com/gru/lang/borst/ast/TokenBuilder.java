package com.gru.lang.borst.ast;

import java.util.List;

import com.gru.lang.borst.Token;
import com.gru.lang.borst.TokenType;

import java.util.ArrayList;

public class TokenBuilder {
	
	List<Token> tokens = new ArrayList<Token>();
	
	public TokenBuilder lparen() {
		tokens.add(new Token(TokenType.lparen, null));
		return this;
	}
	
	public TokenBuilder rparen() {
		tokens.add(new Token(TokenType.rparen, null));
		return this;
	}
	
	public TokenBuilder num(int val) {
		tokens.add(new Token(TokenType.number, val));
		return this;
	}
	
	public TokenBuilder plus(int val) {
		tokens.add(new Token(TokenType.plus, null));
		num(val);
		return this;
	}
	
	public TokenBuilder minus(int val) {
		tokens.add(new Token(TokenType.minus, null));
		num(val);
		return this;
	}
	
	public TokenBuilder mul(int val) {
		tokens.add(new Token(TokenType.mul, null));
		num(val);
		return this;
	}
	
	public TokenBuilder mul() {
		tokens.add(new Token(TokenType.mul, null));
		return this;
	}
	
	public TokenBuilder div(int val) {
		tokens.add(new Token(TokenType.div, null));
		num(val);
		return this;
	}
	
	public List<Token> build() {
		return tokens;
	}
	
	
	

}
